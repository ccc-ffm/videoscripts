#!/usr/bin/python3

from renderlib import *
from easing import *

def introFrames(p):
	move=50

	yield (
		('logo', 'style', 'opacity', '0'),
		('club', 'style', 'opacity', '0'),
		('id', 'style', 'opacity', '0'),
		('title', 'style', 'opacity', '0'),
		('person', 'style', 'opacity', '0'),
	)

	# 2 Sekunde Logo FadeIn
	frames = 1*fps
	for i in range(0, frames):
		yield (
			('logo', 'style', 'opacity', "%.4f" % easeLinear(i, 0, 1, frames)),
		)

	# 1 Sekunde club FadeIn
	frames = 1*fps
	for i in range(0, frames):
		yield (
			('club', 'style', 'opacity', "%.4f" % easeLinear(i, 0, 1, frames)),
		)

	# 2 Sekunde id FadeIn
	frames = 1*fps
	for i in range(0, frames):
		yield (
			('id', 'style', 'opacity', "%.4f" % easeLinear(i, 0, 1, frames)),
		)

	# 1 Sekunde titel Fadein
	frames = 1*fps
	for i in range(0, frames):
		yield (
			('title', 'style',    'opacity', "%.4f" % easeLinear(i, 0, 1, frames)),
			#('title', 'attr',     'transform', 'translate(%.4f, 0)' % easeOutQuad(i, -move, move, frames)),
		)

	# 1 Sekunde Person einfaden
	frames = 1*fps
	for i in range(0, frames):
		yield (
			('person', 'style',    'opacity', "%.4f" % easeLinear(i, 0, 1, frames)),
			#('person', 'attr',     'transform', 'translate(%.4f, 0)' % easeOutQuad(i, -move, move, frames)),
		)


	# 3 Sekunden stehen lassen
	frames = 3*fps
	for i in range(0, frames):
		yield ()

	# 2 Sekunden alles ausfaden
	frames = 2*fps
	for i in range(0, frames):
		yield (
			('person', 'style',    'opacity', "%.4f" % easeLinear(i, 1, -1, frames)),
			('logo', 'style',    'opacity', "%.4f" % easeLinear(i, 1, -1, frames)),
			('club', 'style',    'opacity', "%.4f" % easeLinear(i, 1, -1, frames)),
			('id', 'style',    'opacity', "%.4f" % easeLinear(i, 1, -1, frames)),
			('title', 'style',    'opacity', "%.4f" % easeLinear(i, 1, -1, frames)),
		)

	# 2 leere Frames zum Abschluss
	for i in range(0, 2):
		yield ()


def outroFrames(p):
	# 5 Sekunden stehen bleiben
	frames = 5*fps
	for i in range(0, frames):
		yield []

def debug():
	render(
		'intro.svg',
		'../intro.ts',
		introFrames,
		{
			'$type': 'Vortrag',
			'$date': 'yyyy-mm-dd',
			'$title': 'Titel des Vortrags',
			'$person': 'speaker';
		}
	)

	render(
		'outro.svg',
		'../outro.ts',
		outroFrames
	)

def tasks(queue, args):
	raise NotImplementedError('call with --debug to render your intro/outro')
