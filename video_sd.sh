#!/bin/bash

out=${1%.*}-sd.mp4
ffmpeg -threads 0 -analyzeduration 20000000 -i $1 \
-filter_complex hqdn3d,scale=512:288 \
-c:v:0 libx264 -bufsize:0 8192k -minrate:0 50k -maxrate:0 900k -crf:0 22 \
-profile:0 baseline -c:a:0 aac -strict -2 -b:a:0 96k -ac:a:0 2 -ar:a:0 48000 \
-aspect 16:9 -pix_fmt yuv420p -f mp4 $out

