#!/bin/bash

target=$(basename ${1%.*})-hd.mp4

ffmpeg -y -nostdin -threads 4 -analyzeduration 20000000 \
-i $1 -filter_complex "[0:0] hqdn3d,drawbox=0:0:720:1:black [vd]" \
-map "[vd]" \
-c:v:0 libx264 -bufsize:0 8192k -minrate:0 100k -maxrate:0 2000k \
-crf:v 18 -profile:0 main \
-map 0:1 \
-c:a:0 aac -strict -2 -b:a:0 128k -ac:a:0 2 -ar:a:0 48000 \
-aspect 16:9 -f mp4 \
$target

