#!/usr/bin/python3

import os
import argparse
import re
import sys
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument("-f", metavar='filename.mp4', type=str, help="output filename", default="output.mp4")
parser.add_argument("-i", metavar='intro.ts', type=str, help="Introfile")
parser.add_argument("-o", metavar='outro.ts', type=str, help="Outrofile")
parser.add_argument("-s", metavar='hh:mm:ii', type=str, help="skip hh:mm:ss of main video")
parser.add_argument("-d", help="debug", action="store_true", default=False)
parser.add_argument("-n", help="normalize", action="store_true", default=False)
parser.add_argument("--dry", help="dry run, just prentend doing anything", action="store_true", default=False)
#parser.add_argument("-c", metavar='c', type=int, help="cut last n seconds of main video")
parser.add_argument("videofiles", type=str, help="files from camera in correct order", nargs='+')
args = parser.parse_args()

filename = args.f

if args.d:
    debug = True
else:
    debug = False

if args.n:
    normalize = True
else:
    normalize = False

if args.dry:
    dry = True
    debug = True
else:
    dry = False

if args.s and re.fullmatch("^\d\d:\d\d:\d\d$", args.s) == None:
    sys.stderr.write("-s skip time must be given in the form hh:mm:ss with leading zeros. e.g.: 00:01:10 to skip the first 70 Seconds\n")
    sys.exit(1)

ownpath = os.path.split(os.path.abspath(__file__))
ownpath = ownpath[0]

filebase = filename.split(".")
filebase = ".".join(filebase[0:-1])

videofiles = args.videofiles

rawstring= "|".join(args.videofiles)

""" normalize and concat mts """
if args.n:
    getnormcli = "ffmpeg -i 'concat:%s' -af 'volumedetect' -f null /dev/null" % (rawstring)
    print()
    print("Ermittele Normalisierungsinformationen. Das kann dauern!")
    if debug:
        print(getnormcli)
    r = os.popen(getnormcli + " 2>&1 | fgrep max_volume").readlines()
    max_vol=r[0]
    max_vol = max_vol.split()
    max_vol = float(max_vol[-2])
    norm_vol = max_vol * -1
    print("Maximale Lautstärke: %.1fdB also muss die Lautstärke um %.1fdB angepasst werden" % (max_vol, norm_vol))

    maketmp = "ffmpeg -y -i 'concat:%s' -vcodec copy -map_channel 0.1.0 -map_channel 0.1.0 -af 'volume=%.1fdB' -c:a aac -strict -2 tmp.mp4" % (rawstring, norm_vol)
else:
    maketmp = "ffmpeg -y -i 'concat:%s' -vcodec copy -map_channel 0.1.0 -map_channel 0.1.0 -strict -2 tmp.mp4" % (rawstring)

print("Mixe Videofragemente zusammen... Das dauert!")
if debug:
    print(maketmp)

if not dry:
    os.system(maketmp + ('' if debug else ' > /dev/null 2>&1'))

""" preppend intro append outro """
io = 0
video = []
if args.i:
    io += 1
    video.append("-i")
    video.append(args.i)
if args.s:
    video.append("-ss")
    video.append(args.s)
video.append('-i')
video.append('tmp.mp4')
if args.o:
    io += 1
    video.append("-i")
    video.append(args.o)
videostring = " ".join(video)

""" mapping to preppend intro and append outro """
maps = []
vcount = 1 + io
for i in range(0,vcount):
    maps.append("[%i:0] [%i:1]" % (i, i))
maps.append("concat=n=%i:v=1:a=1 [v] [a]" % (vcount))
mapstring = " ".join(maps)

""" generate raw video file """
makeraw = "ffmpeg -threads 16 -analyzeduration 20000000 %s -filter_complex '%s' -map '[v]' -map '[a]' -strict -2 %s" % (videostring, mapstring, filename)

print("Erzeuge HD-Master... Das dauert!")
if debug:
    print(makeraw)
if not dry:
    os.system(makeraw + ('' if debug else ' > /dev/null 2>&1'))

""" export mp3 """
print("Exportiere MP3. Kurz und schmerzlos!")
makemp3 = "ffmpeg -i %s %s.mp3" % ("tmp.mp4", filebase)
if debug:
    print(makemp3)
if not dry:
    os.system(makemp3 + ('' if debug else ' > /dev/null 2>&1'))

""" export opus """
print("Exportiere OPUS. Kurz und schmerzlos!")
makeopus = "ffmpeg -i %s %s.opus" % ("tmp.mp4", filebase)
if debug:
    print(makeopus)
if not dry:
    os.system(makeopus + ('' if debug else ' > /dev/null 2>&1'))

""" remove temporary file """
print("Entferne temporäre Datei tmp.mp4")
if debug:
    print("rm tmp.mp4")
if not dry:
    os.system("rm tmp.mp4")

""" encode h264 hd """
print("Encodiere HD-Video in h264... Das dauert!")
makeh264hd = ownpath + "/video_hd.sh " + filename + ('' if debug else ' > /dev/null 2>&1')
if debug:
    print(makeh264hd)
if not dry:
    os.system(makeh264hd)
h264hd = filebase + "-hd.mp4"

""" encode h264 sd """
print("Encodiere SD-Video in h264... Das geht so!")
makeh264sd = ownpath + "/video_sd.sh " + filename + ('' if debug else ' > /dev/null 2>&1')
if debug:
    print(makeh264sd)
if not dry:
    os.system(makeh264sd)
h264sd = filebase + "-sd.mp4"

""" encode webm hd """
print("Encodiere HD-Video in webm... Das dauert richtig lange!")
makewebmhd = ownpath + "/webm.sh " + h264hd + ('' if debug else ' > /dev/null 2>&1')
if debug:
    print(makewebmhd)
if not dry:
    os.system(makewebmhd)

""" encode webm sd """
print("Encodiere SD-Video in webm... Das dauert!")
makewebmsd = ownpath + "/webm.sh " + h264sd + ('' if debug else ' > /dev/null 2>&1')
if debug:
    print(makewebmsd)
if not dry:
    os.system(makewebmsd)

""" remove hd master """
print("Entferne HD-Master")
rmhdmaster = "rm " + filename
if debug:
    print(rmhdmaster)
if not dry:
    os.system(rmhdmaster)

print("Fertig. Danke das du mit uns gefolgen bist!")
