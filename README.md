# cccffm2mediacccde

Tools von media.ccc.de zum Veröffentlichen von Videos auf media.ccc.de gepaart mit dem Artwork für den CCC FFM. 

## Abhängigkeiten

* python3
* python3-lxml
* python3-cssutils
* ffmpeg
* acc
* x.264

## HowTo

Titel, Autor, Datum, etc. in der Datei `cccffm/__init__.py` anpassen. Danach Intro und Outro mit dem Befehl

```python
./make.py cccffm --debug
```

generieren. Die entstandenden TS-Streams vor bzw. hinter das Video schneiden und das ganze mit den scripten video_hd.sh, video_sd.sh & web.sh encoden. Das Encoding dauert eine ganze Weile, speziell bei webm, da hier zwei Durchläufe pro Auflösung nötig sind.
